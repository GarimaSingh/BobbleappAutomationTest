package com.example.garimasingh.bobbleautomation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

//import com.touchtalent.bobbleapp.R;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class BobbleKeyboardSettings {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<MainActivity>(MainActivity.class);

    private UiDevice mDevice;

    @Before
    public void setUp() {
        // Initialize UiDevice instance

        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        // Start from the home screen
        //mDevice.pressHome();

        //mDevice.wait(Until.hasObject(By.pkg(getLauncherPackageName()).depth(0)), 1000);
    }

    @Test
    public void checkSettings() throws UiObjectNotFoundException {

        // Simulate a short press on the HOME button.
        /*mDevice.pressHome();

        // We’re now in the home screen. Next, we want to simulate
        // a user bringing up the All Apps screen.
        // If you use the uiautomatorviewer tool to capture a snapshot
        // of the Home screen, notice that the All Apps button’s
        // content-description property has the value “Apps”. We can
        // use this property to create a UiSelector to find the button.

        UiObject bobbleappopen = mDevice.findObject(new UiSelector().text("Bobble"));

        bobbleappopen.clickAndWaitForNewWindow();*/

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject cancel1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/cancel"));

        if (cancel1.exists()) {
            cancel1.click();

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        UiObject enable1 = mDevice.findObject(new UiSelector().text("LATER"));

        if (enable1.exists()) {
            enable1.click();
        }

        UiObject tap1 = mDevice.findObject(new UiSelector().text("Tap to Share"));

        if (tap1.exists()) {
            tap1.click();
        }

        tap1 = mDevice.findObject(new UiSelector().text("Got it"));

        if (tap1.exists()) {
            tap1.click();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject menu1 = mDevice.findObject(new UiSelector().description("drawer open"));

        menu1.click();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject settings1 = mDevice.findObject(new UiSelector().text("Bobble Keyboard"));

        settings1.clickAndWaitForNewWindow();

        enable1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

        if (enable1.exists()) {
            enable1.click();

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            enable1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

            if (enable1.exists()) {
                enable1.clickAndWaitForNewWindow();
            }

            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            enable1 = mDevice.findObject(new UiSelector().text("LATER"));

            if (enable1.exists()) {
                enable1.click();

                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }

            menu1 = mDevice.findObject(new UiSelector().description("drawer open"));

            menu1.click();

            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }

            settings1 = mDevice.findObject(new UiSelector().text("Bobble Keyboard"));

            settings1.clickAndWaitForNewWindow();
        }

        UiObject key1 = mDevice.findObject(new UiSelector().text("Keyboard Display and Sounds"));

        key1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject key11 = mDevice.findObject(new UiSelector().index(3));

        key11.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().index(5));

        key11.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject navup1 = mDevice.findObject(new UiSelector().description("Navigate up"));

        navup1.clickAndWaitForNewWindow();

        key1 = mDevice.findObject(new UiSelector().text("Typing"));

        key1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/autospellingcheck"));

        key11.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));

        appViews.flingToEnd(2);

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/gestureDeleteCheck"));

        key11.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDevice.pressBack();

        key1 = mDevice.findObject(new UiSelector().text("Languages"));

        key1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiScrollable appViews1 = new UiScrollable(new UiSelector().scrollable(true));
        appViews1.setAsHorizontalList();

        UiObject language1 = appViews1.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()), "Bengali");

        language1.click();

        //30 seconds delay added to incorporate downloading of the selected language.
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject check1 = mDevice.findObject(new UiSelector().text("Tryout the keyboard"));

        if (check1.exists()) {
            check1.click();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        check1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/nativeImage"));

        if (check1.exists()) {
            check1.click();
        }

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        language1 = appViews1.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()), "Malayalam");

        language1.click();

        //30 seconds delay added to incorporate downloading of the selected language.
        try {
            Thread.sleep(30000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        check1 = mDevice.findObject(new UiSelector().text("Tryout the keyboard"));

        if (check1.exists()) {
            check1.click();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDevice.pressBack();

        check1 = mDevice.findObject(new UiSelector().text("Uninstall Language"));

        if (check1.exists()) {
            check1.click();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key1 = mDevice.findObject(new UiSelector().text("Themes"));

        if (!key1.exists()) {
            mDevice.pressBack();
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        key1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        // No unique ID found for themes.
        /*key11 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/themes"));

        key11.click();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().text("Photos"));

        key11.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().text("InfiniteLoop"));

        key11.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().resourceId("com.google.android.apps.photos:id/list_photo_tile_view"));

        key11.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().text("NEXT"));

        key11.clickAndWaitForNewWindow();

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().text("Done"));

        key11.clickAndWaitForNewWindow();*/

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        check1 = mDevice.findObject(new UiSelector().text("Tryout the keyboard"));

        if (check1.exists()) {
            check1.click();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        key11 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/keyBordertoggle"));

        if (key11.exists()) {
            key11.click();
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDevice.pressBack();

        mDevice.pressBack();
        mDevice.pressBack();
    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

}