package com.example.garimasingh.bobbleautomation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class GIFCatalog {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<MainActivity>(MainActivity.class);

    private UiDevice mDevice;

    @Before
    public void setUp() {

        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @Test
    public void checkSettings() throws UiObjectNotFoundException {

        performcheck();

        UiObject gif1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/radioBtnGif"));

        gif1.click();

        timedelay5();

        enablekeyboard();

        performcheck();

        gif1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/radioBtnGif"));

        gif1.click();

        timedelay5();

        UiObject store1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/catalogLayout"));

        store1.click();

        timedelay10();
        timedelay10();

        store1 = mDevice.findObject(new UiSelector().text("com.touchtalent.bobbleapp:id/time_remaining"));

        if(!store1.exists())
        {
            UiObject search1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/downloadGifPack"));

            search1.click();

            timedelay10();
            timedelay10();
        }

        else
        {
            store1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/catalogLayout"));

            store1.click();
        }

        mDevice.pressBack();
        mDevice.pressBack();
    }

    public void performcheck() throws UiObjectNotFoundException{

        UiObject cancel1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/cancel"));

        if(cancel1.exists())
        {
            cancel1.click();

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        UiObject enable1 = mDevice.findObject(new UiSelector().text("LATER"));

        if(enable1.exists())
        {
            enable1.click();
        }

        UiObject tap1 = mDevice.findObject(new UiSelector().text("Tap to Share"));

        if(tap1.exists())
        {
            tap1.click();
        }

        tap1 = mDevice.findObject(new UiSelector().text("Got it"));

        if(tap1.exists())
        {
            tap1.click();
        }
    }

    public void timedelay5() throws UiObjectNotFoundException{

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void timedelay3() throws UiObjectNotFoundException{

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void timedelay10() throws UiObjectNotFoundException{

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void enablekeyboard() throws UiObjectNotFoundException{

        UiObject select1 = mDevice.findObject(new UiSelector().textContains("ENABLE"));

        if(select1.exists())
        {
            select1.click();

            timedelay5();

            UiObject enable1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

            if(enable1.exists())
            {
                enable1.click();
                timedelay5();
            }

            enable1 = mDevice.findObject(new UiSelector().text("OK"));

            if(enable1.exists())
            {
                enable1.clickAndWaitForNewWindow();
                timedelay5();
            }

            timedelay10();
        }

        select1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

        if(select1.exists())
        {
            select1.click();

            timedelay5();

            select1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));
            select1.click();

            timedelay10();
        }
    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

}
