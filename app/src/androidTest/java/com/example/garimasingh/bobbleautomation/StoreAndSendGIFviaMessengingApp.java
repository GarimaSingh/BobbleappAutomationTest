package com.example.garimasingh.bobbleautomation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class StoreAndSendGIFviaMessengingApp {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<MainActivity>(MainActivity.class);

    private UiDevice mDevice;

    @Before
    public void setUp() {
        // Initialize UiDevice instance

        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
        // Start from the home screen
        //mDevice.pressHome();

        //mDevice.wait(Until.hasObject(By.pkg(getLauncherPackageName()).depth(0)), 1000);
    }

    @Test
    public void checkSettings() throws UiObjectNotFoundException {

        // Simulate a short press on the HOME button.
        /*mDevice.pressHome();

        // We’re now in the home screen. Next, we want to simulate
        // a user bringing up the All Apps screen.
        // If you use the uiautomatorviewer tool to capture a snapshot
        // of the Home screen, notice that the All Apps button’s
        // content-description property has the value “Apps”. We can
        // use this property to create a UiSelector to find the button.

        UiObject bobbleappopen = mDevice.findObject(new UiSelector().text("Bobble"));

        bobbleappopen.clickAndWaitForNewWindow();*/

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject cancel1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/cancel"));

        if(cancel1.exists())
        {
            cancel1.click();

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        UiObject enable1 = mDevice.findObject(new UiSelector().text("LATER"));

        if(enable1.exists())
        {
            enable1.click();
        }

        UiObject tap1 = mDevice.findObject(new UiSelector().text("Tap to Share"));

        if(tap1.exists())
        {
            tap1.click();
        }

        tap1 = mDevice.findObject(new UiSelector().text("Got it"));

        if(tap1.exists())
        {
            tap1.click();
        }

        UiObject gif1 = mDevice.findObject(new UiSelector().text("GIF"));

        gif1.click();

        try {
            Thread.sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject enablekey = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

        if(enablekey.exists()){

            enablekey.click();

            UiObject select1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

            select1.clickAndWaitForNewWindow();
        }

        UiObject gif11 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/itemImageView"));

        gif11.click();

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject save1 = mDevice.findObject(new UiSelector().text("Save in Gallery"));

        save1.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*UiObject message1 = mDevice.findObject(new UiSelector().text("Messaging"));

        message1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject contact1 = mDevice.findObject(new UiSelector().resourceId("com.android.mms:id/contact_pick_button"));

        contact1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject select1 = mDevice.findObject(new UiSelector().text("Distress Number"));

        select1.click();

        UiScrollable appViews = new UiScrollable(new UiSelector().scrollable(true));
        UiObject select2 = appViews.getChildByText(new UiSelector().className(android.widget.TextView.class.getName()),"235-689");

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }*/

        //select2.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*UiObject done1 = mDevice.findObject(new UiSelector().resourceId("com.android.contacts:id/done_icon"));

        done1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject done2 = mDevice.findObject(new UiSelector().resourceId("android:id/up"));

        done2.clickAndWaitForNewWindow();*/

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDevice.pressBack();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDevice.pressBack();
        mDevice.pressBack();
    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }

}
