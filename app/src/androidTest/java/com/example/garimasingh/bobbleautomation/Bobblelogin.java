package com.example.garimasingh.bobbleautomation;

import android.content.Context;
import android.content.Intent;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;
import android.util.Log;
import android.widget.EditText;

import org.junit.Before;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;

@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class Bobblelogin {
    public static final String APP_PACKAGE_NAME = "com.touchtalent.bobbleapp";
    public static final int LAUNCH_TIMEOUT = 5000;
    public static final String TEST_NUMBER = "8448518606";
    private UiDevice mDevice;


    @Before
    public void before() throws Exception {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        mDevice.pressHome();

        //To Launch the Launcher Application
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)),
                LAUNCH_TIMEOUT);

        //To Launch Application with Package Name
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(APP_PACKAGE_NAME);
        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        //Waiting App to Come on Screen
        mDevice.wait(Until.hasObject(By.pkg(APP_PACKAGE_NAME).depth(0)),
                LAUNCH_TIMEOUT);

    }

    @org.junit.Test
    public void Test() throws InterruptedException, UiObjectNotFoundException {

        Thread.sleep(3000);

        // allow permissions
        UiObject acceptButton = mDevice.findObject(new UiSelector().text("ACCEPT"));
        if (acceptButton.exists()) {
            acceptButton.click();
            Log.i("Working", "Chal Rha hai");
        }

        UiObject allowMedia = mDevice.findObject(new UiSelector()
                .text("Allow")
                .resourceId("com.android.packageinstaller:id/permission_allow_button")
                .className("android.widget.Button"));
        if (allowMedia.exists() && allowMedia.isClickable()) {
            allowMedia.click();
        }

        UiObject allowKeyboard = mDevice.findObject(new UiSelector()
                .text("Allow")
                .resourceId("com.android.packageinstaller:id/permission_allow_button")
                .className("android.widget.Button"));
        if (allowKeyboard.exists() && allowKeyboard.isClickable()) {
            allowKeyboard.click();
        }

        UiObject allowSMS = mDevice.findObject(new UiSelector().text("Allow")
                .resourceId("com.android.packageinstaller:id/permission_allow_button")
                .className("android.widget.Button"));
        if (allowSMS.exists() && allowSMS.isClickable()) {
            allowSMS.click();
        }

        //Click login using truecaller button
        UiObject2 oneTap = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp", "button_cloudLoginActivity_truecallerLoginButton"))
                , 10000);
        oneTap.click();

        //Login using Truecaller
        UiObject2 Continue = mDevice.wait(Until.findObject(By.res("com.truecaller", "confirm")),
                4000);
        Continue.click();

        //Enable Keyboard
        UiObject2 enableKeyboard = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp", "enableKBButton")), 3000);
        enableKeyboard.click();

        //Enable kb from android settings
        Thread.sleep(2000);
        UiObject systemEnableKeyboard = mDevice.findObject(new UiSelector().className("android.widget.TextView")
                .text("Bobble Keyboard ✪"));
        if (systemEnableKeyboard.exists()) {
            systemEnableKeyboard.click();
        }
//        Thread.sleep(6000);
//
//        //Confirm Reboot Warning
//        UiObject afterReboot = mDevice.findObject(new UiSelector().
//                text("OK").className("android.widget.Button")
//                .resourceId("android:id/button1"));
//        if (afterReboot.exists() && afterReboot.isClickable()) {
//            afterReboot.click();
//        }
         Thread.sleep(5000);


        //click ok on system warning
        UiObject okbutton = mDevice.findObject(new UiSelector().text("OK"));
        if(okbutton.exists()){
            okbutton.click();
        } Thread.sleep(3000);


        //Enable Bobble Keyboard
        UiObject SelectBobbleKeyboard = mDevice.findObject(new UiSelector().resourceId("android:id/text1")
                .className("android.widget.TextView").checked(false).text("Bobble Keyboard ✪"));
        if (SelectBobbleKeyboard.exists()) {

            SelectBobbleKeyboard.click();

        }
        Thread.sleep(5000);

        //Welcome prompt
        UiObject2 Welcome = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp", "continueTextView")), 1000);
        Welcome.click();


        Thread.sleep(2000);
        //Grant Location Access
        UiObject locationAccess = mDevice.findObject(new UiSelector().
                text("Allow").resourceId("com.android.packageinstaller:id/permission_allow_button")
                .className("android.widget.Button"));
        if (locationAccess.exists() && locationAccess.isClickable()) {
            locationAccess.click();
        }
    }
}


