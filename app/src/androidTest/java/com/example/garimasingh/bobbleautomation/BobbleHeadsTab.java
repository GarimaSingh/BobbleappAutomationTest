package com.example.garimasingh.bobbleautomation;
import android.content.Context;
import android.content.Intent;
import android.provider.Contacts;
import android.support.test.InstrumentationRegistry;
import android.support.test.filters.SdkSuppress;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObject2;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.junit.Before;
import org.junit.runner.RunWith;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.IsNull.notNullValue;


@RunWith(AndroidJUnit4.class)
@SdkSuppress(minSdkVersion = 18)
public class BobbleHeadsTab {

    public static final String APP_PACKAGE_NAME = "com.touchtalent.bobbleapp";
    public static final int LAUNCH_TIMEOUT = 5000;
    public static final String TEST_NUMBER = "9312100736";
    private UiDevice mDevice;

    @Before
    public void before() throws Exception {
        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());

        mDevice.pressHome();

        //To Launch the Launcher Application
        final String launcherPackage = mDevice.getLauncherPackageName();
        assertThat(launcherPackage, notNullValue());
        mDevice.wait(Until.hasObject(By.pkg(launcherPackage).depth(0)),
                LAUNCH_TIMEOUT);

        //To Launch Application with Package Name
        Context context = InstrumentationRegistry.getContext();
        final Intent intent = context.getPackageManager()
                .getLaunchIntentForPackage(APP_PACKAGE_NAME);
        // Clear out any previous instances
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        context.startActivity(intent);

        //Waiting App to Come on Screen
        mDevice.wait(Until.hasObject(By.pkg(APP_PACKAGE_NAME).depth(0)),
                LAUNCH_TIMEOUT);

    }

    @org.junit.Test
    public void Test() throws InterruptedException, UiObjectNotFoundException {
        Thread.sleep(6000);

        //To click camera icon
        UiObject2 cameraicon = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp", "createNewPersonalHead"))
                ,20000);
        cameraicon.click();

//       //allow permission access to camera
//        UiObject2 allowbutton = mDevice.wait(Until.findObject(By.res("com.google.android.packageinstaller","permission_allow_button"))
//             ,4000);
//        allowbutton.click();

        //To click picture button
        UiObject2 picturebutton = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","takePictureButton"))
                ,3000);
        picturebutton.click();

        //To click gender button
        UiObject2 gender = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","btnFemale1"))
                ,20000);
        gender.click();

        Thread.sleep(9000);
        //To click next buttoon
        UiObject2 next = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","next")),3000);
        next.click();
        Thread.sleep(5000);

        //To click heads tab
        UiObject2 Heads = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","textInCustomTab"))
                ,1000);
        Heads.click();

        //To click Head
        UiObject2 Imageview = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","itemImageView"))
                ,3000);
        Imageview.click();

        //Update relation
        UiObject2 relation = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","addRelation"))
                ,3000);
        relation.click();

        Thread.sleep(3000);
        //Choose relationship
        UiObject chooserelation = mDevice.findObject(new UiSelector().text("Friend"));
        if(chooserelation.exists()) {
            chooserelation.click();
        } Thread.sleep(3000);

        //Share head with friend
        UiObject2 Imageview1 = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","itemImageView"))
                ,3000);
        Imageview1.click();

        UiObject2 sharehead = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","share_connection_adapter_icon"))
                ,3000);
        sharehead.click();

        //To download heads from suggestion
        UiObject2 addhead = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","add_head_button"))
                ,3000);
        sharehead.click();

        //To remove relation button
        UiObject2 remove = mDevice.wait(Until.findObject(By.res("com.touchtalent.bobbleapp","remove_relation_button"))
                ,3000);
       remove.click();

















    }
}

