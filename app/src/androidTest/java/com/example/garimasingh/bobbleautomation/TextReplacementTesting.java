package com.example.garimasingh.bobbleautomation;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.test.uiautomator.By;
import android.support.test.uiautomator.UiDevice;
import android.support.test.uiautomator.UiObject;
import android.support.test.uiautomator.UiObjectNotFoundException;
import android.support.test.uiautomator.UiScrollable;
import android.support.test.uiautomator.UiSelector;
import android.support.test.uiautomator.Until;

import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class TextReplacementTesting {

    @Rule
    public ActivityTestRule<MainActivity> mActivityRule
            = new ActivityTestRule<MainActivity>(MainActivity.class);

    private UiDevice mDevice;

    @Before
    public void setUp() {
        // Initialize UiDevice instance

        mDevice = UiDevice.getInstance(InstrumentationRegistry.getInstrumentation());
    }

    @Test
    public void checkSettings() throws UiObjectNotFoundException {

        performcheck();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject menu1 = mDevice.findObject(new UiSelector().description("drawer open"));

        menu1.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject settings1 = mDevice.findObject(new UiSelector().text("Bobble Keyboard"));

        settings1.clickAndWaitForNewWindow();

        enablekeyboard();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject typing1 = mDevice.findObject(new UiSelector().text("Typing"));

        typing1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject typing2 = mDevice.findObject(new UiSelector().text("Text Replacement (Shortcuts)"));

        typing2.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject shortcut1 = mDevice.findObject(new UiSelector().text("Shortcut(optional)"));

        shortcut1.clearTextField();

        shortcut1.setText("lol");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        shortcut1 = mDevice.findObject(new UiSelector().text("Phrase or word(Max: 48 characters)"));

        shortcut1.clearTextField();

        shortcut1.setText("Laughing Out Loud");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        shortcut1 = mDevice.findObject(new UiSelector().text("save"));

        if (shortcut1.isClickable()) {
            shortcut1.click();
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        UiObject navup1 = mDevice.findObject(new UiSelector().description("Navigate up"));

        navup1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        navup1 = mDevice.findObject(new UiSelector().description("Navigate up"));

        navup1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        navup1 = mDevice.findObject(new UiSelector().description("Navigate up"));

        navup1.clickAndWaitForNewWindow();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        shortcut1 = mDevice.findObject(new UiSelector().text("Type your message here..."));

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        shortcut1.click();

        shortcut1.setText("lol");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        mDevice.pressBack();
        mDevice.pressBack();
    }

    public void enablekeyboard() throws UiObjectNotFoundException {

        UiObject select1 = mDevice.findObject(new UiSelector().textContains("ENABLE"));

        if (select1.exists()) {
            select1.click();

            timedelay5();

            UiObject enable1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

            if (enable1.exists()) {
                enable1.click();
                timedelay5();
            }

            enable1 = mDevice.findObject(new UiSelector().text("OK"));

            if (enable1.exists()) {
                enable1.clickAndWaitForNewWindow();
                timedelay5();
            }

            timedelay10();
        }

        select1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

        if (select1.exists()) {
            select1.click();

            timedelay5();
        }

        select1 = mDevice.findObject(new UiSelector().textContains("Bobble Keyboard"));

        if (select1.exists()) {
            select1.click();

            timedelay10();
        }

        performcheck();

        UiObject menu1 = mDevice.findObject(new UiSelector().description("drawer open"));

        menu1.click();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        UiObject settings1 = mDevice.findObject(new UiSelector().text("Bobble Keyboard"));

        settings1.clickAndWaitForNewWindow();
    }

    public void timedelay5() throws UiObjectNotFoundException {

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void timedelay3() throws UiObjectNotFoundException {

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void timedelay10() throws UiObjectNotFoundException {

        try {
            Thread.sleep(10000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public void performcheck() throws UiObjectNotFoundException {

        UiObject cancel1 = mDevice.findObject(new UiSelector().resourceId("com.touchtalent.bobbleapp:id/cancel"));

        if (cancel1.exists()) {
            cancel1.click();

            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

        UiObject enable1 = mDevice.findObject(new UiSelector().text("LATER"));

        if (enable1.exists()) {
            enable1.click();
        }

        UiObject tap1 = mDevice.findObject(new UiSelector().text("Tap to Share"));

        if (tap1.exists()) {
            tap1.click();
        }

        tap1 = mDevice.findObject(new UiSelector().text("Got it"));

        if (tap1.exists()) {
            tap1.click();
        }
    }

    private String getLauncherPackageName() {
        // Create launcher Intent
        final Intent intent = new Intent(Intent.ACTION_MAIN);
        intent.addCategory(Intent.CATEGORY_HOME);

        // Use PackageManager to get the launcher package name
        PackageManager pm = InstrumentationRegistry.getContext().getPackageManager();
        ResolveInfo resolveInfo = pm.resolveActivity(intent, PackageManager.MATCH_DEFAULT_ONLY);
        return resolveInfo.activityInfo.packageName;
    }
}
